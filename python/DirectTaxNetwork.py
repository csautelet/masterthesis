# Author: Caroline Sautelet
# Date: 02-02-2017
########################################################
# DirectTaxNetwork Class:
# Create adjacency matrix of the tax graph based on data
# files
# Compute properties of this tax network for direct paths
# NO STRATEGIC BEHAVIOR here
########################################################
import networkx as nx
import numpy as np
import math

class DirectTaxNetwork():

    # INPUT:
    # - INPUT_FILE: csv, sep = ; one line per country
    # Country Name ; CIT rate; % IN interests taxed;
    # % IN dividends taxed; % IN royalties taxed; EU
    #
    # - whtI: csv, sep = ; representing a N by N matrix
    # first line = header name of country
    # first column =  header name of country
    # entry i,j is the withholding tax rate on
    # Interests, dividends or royalties (resp. for whtI, whtD, whtR)
    # for sending from country i to j
    # PB : 1 if use Patent Box regimes, 0 otherwise
    def __init__(self,INPUT_FILE,whtI,whtD,whtR,PB):
        # networkx graph
        self.graph = nx.DiGraph()
        # key: node ID from 1 to 6*N
        # value: country name
        self.node2Country = dict()
        # key: node ID from 1 to 6*N
        # value: Vehicle name
        self.node2Vehicle = dict()
        # key: node ID from 1 to 6*N
        # value: 'In' or 'Out'
        self.node2Way = dict()
        # key: country name
        # value: ID of the first node of this country
        self.country2StartIndex = dict()
        # key: country ID form 0 to (N-1)
        # value: country name
        self.country2Name = dict()
        # key: country ID form 1 to N
        # value: CIT rate
        self.cit = dict()
        # key: country ID form 1 to N
        # value: percentage of incoming dividends taxed
        self.xmp = dict()
        # key: country ID form 1 to N
        # value: percentage of received royalties taxed
        self.pb = dict()
        # key: country ID form 1 to N
        # value: 1 if EU Member State, 0 otherwise
        self.eu = dict()

        n_nodes = 0
        n_countries = 0
        index_TH = []
        div_exemption = dict()
        cit = dict()
        # Read file containing info about each country
        with open(INPUT_FILE, 'r') as f:
            for line in f: #for each country
                n_countries += 1
                s = line.split(';')
                # add nodes to networkx graph
                self.graph.add_nodes_from(range(n_nodes+1,n_nodes+7))
                self.node2Country.update(dict.fromkeys(range(n_nodes+1,n_nodes+7),s[0]))

                self.node2Vehicle.update(dict.fromkeys([n_nodes+1,n_nodes+4],'interest'))
                self.node2Vehicle.update(dict.fromkeys([n_nodes+2,n_nodes+5],'dividend'))
                self.node2Vehicle.update(dict.fromkeys([n_nodes+3,n_nodes+6],'royalties'))

                self.node2Way.update(dict.fromkeys(range(n_nodes+1,n_nodes+4),'In'))
                self.node2Way.update(dict.fromkeys(range(n_nodes+4,n_nodes+7),'Out'))

                self.country2StartIndex[s[0]] = n_nodes+1

                self.country2Name[n_countries-1] = s[0]
                self.cit[n_countries] = float(s[1])
                self.xmp[n_countries] = float(s[3])
                self.pb[n_countries] = float(s[4])
                self.eu[n_countries] = float(s[5])

                n_nodes += 6
        # Allocate memory
        self.whtI = np.zeros((int(n_nodes/6),int(n_nodes/6)))
        self.whtD = np.zeros((int(n_nodes/6),int(n_nodes/6)))
        self.whtR = np.zeros((int(n_nodes/6),int(n_nodes/6)))
        header = 1
        translate = dict()
        # Read file with the WHT on interests
        with open(whtI,'r') as f:
            for line in f:
                if header:
                    header = 0
                    s = line.split(';')
                    for j in range(1,len(s)):
                        x = s[j].split('\n')
                        translate[j] = x[0]
                    continue
                s = line.split(';')
                i = int(self.country2StartIndex[s[0]]//6)
                for j in range(1,len(s)):

                    k = int(self.country2StartIndex[translate[j]]//6)
                    x = s[j].split('\n')
                    self.whtI[i][k] = float(x[0])
        header = 1
        # Read file with the WHT on dividends
        with open(whtD,'r') as f:
            for line in f:
                if header:
                    header = 0
                    continue
                s = line.split(';')
                i = int(self.country2StartIndex[s[0]]//6)
                for j in range(1,len(s)):
                    k = int(self.country2StartIndex[translate[j]]//6)
                    x = s[j].split('\n')
                    self.whtD[i][k] = float(x[0])
        header = 1
        # Read file with the WHT on royalties
        with open(whtR,'r') as f:
            for line in f:
                if header:
                    header = 0
                    continue
                s = line.split(';')
                i = int(self.country2StartIndex[s[0]]//6)
                for j in range(1,len(s)):
                    k = int(self.country2StartIndex[translate[j]]//6)
                    x = s[j].split('\n')

                    self.whtR[i][k] = float(x[0])

        # Allocate memory for ADJACENCY MATRIX
        distance = np.full((n_nodes,n_nodes),math.inf)

        #with open(INPUT_FILE, 'r') as f:
        #    for line in f:
        for c in range(1,n_countries+1):
            country = self.country2Name[c-1]
            a = self.country2StartIndex[country]//6 -1
            index = self.country2StartIndex[country] - 1
            # link in nodes to out nodes of this country
            distance[index][[index+3,index+5]] = [0]*2
            distance[index][index+4] = np.absolute(np.log(1-self.cit[c]))
            distance[index+1][[index+3,index+5]] = [0]*2
            distance[index+1][index+4] = np.absolute(np.log(1-self.cit[c]*self.xmp[c]))

            if PB == 1:
                distance[index+2][[index+3,index+5]] = [0]*2
                distance[index+2][index+4] = np.absolute(np.log(1-self.cit[c]*self.pb[c]))
                #distance[index+2][index+4] = np.absolute(np.log(1-float(s[1])*float(s[4])))
            else:
                distance[index+2][[index+3,index+5]] = [0]*2
                distance[index+2][index+4] = np.absolute(np.log(1-self.cit[c]))
                #distance[index+2][index+4] = np.absolute(np.log(1-float(s[1])))
            for j in self.node2Vehicle.keys():
                current_country = self.node2Country[j]
                b = self.country2StartIndex[current_country]//6 -1
                if self.node2Way[j] == 'In' and current_country != country:
                    if self.node2Vehicle[j] == 'interest':
                        tmp = self.whtI[a][b]#(1- self.country2EU[current_country])*(1-self.country2TaxHaven[current_country])*float(s[5]) + self.country2TaxHaven[current_country]*float(s[8]) + (1-self.country2EU[s[0]])*float(s[5])
                        distance[index+3][j-1] = np.absolute(np.log(1-tmp))
                    elif self.node2Vehicle[j] == 'dividend':
                        #if index+5 in index_TH:
                        #    tmp = np.absolute(np.log(1-0.4))
                        #else:
                        tmp = self.whtD[a][b]#(1- self.country2EU[current_country])*(1-self.country2TaxHaven[current_country])*float(s[6]) + self.country2TaxHaven[current_country]*float(s[9]) + (1-self.country2EU[s[0]])*float(s[6])
                        distance[index+4][j-1] = np.absolute(np.log(1-tmp))
                    elif self.node2Vehicle[j] == 'royalties':
                        tmp = self.whtR[a][b]#(1- self.country2EU[current_country])*(1-self.country2TaxHaven[current_country])*float(s[7]) + self.country2TaxHaven[current_country]*float(s[10]) + (1-self.country2EU[s[0]])*float(s[7])
                        distance[index+5][j-1] = np.absolute(np.log(1-tmp))

        self.distance = distance

        cost = distance.copy()
        s = distance.shape
        # Transform the cost of each edge
        for i in range(0,s[0]):
            for j in range(0,s[1]):
                cost[i][j] = 1 - np.exp(-distance[i][j])
                self.cost = cost

        # We now have the distance matrix let s build the networkx graph
        for i in range(1,n_nodes+1):
            string = self.node2Country[i] + "\n" +self.node2Vehicle[i] + "\n"+self.node2Way[i]
            self.graph.add_node(i,label = string)
        for src in range(0,n_nodes):
            for dst in range(0,n_nodes):
                if distance[src][dst] != math.inf:
                    if src == 96 and dst == 100:
                        print("cost")
                        print(distance[src][dst])
                    self.graph.add_weighted_edges_from([(src+1,dst+1,distance[src][dst])])
        self.taxAtSource()
        self.totalTax()
    # Create a N by N matrix where entry i,j is the total tax
    # paid in country i (at source) when sending payment to j
    # WITHOUT STRATEGIC BEHAVIOR
    def taxAtSource(self):
        N = len(list(self.country2StartIndex))
        self.taxAtSource = np.full((N,N),0.0)
        for source in range(0,N):
            for target in range(0,N):
                # find path from source to target
                i = source*6
                j = target*6+4
                distance = self.distance[i][i+4] + self.distance[i+4][j-4+1]
                self.taxAtSource[source][target] = 1-np.exp(-distance)
    # Create a N by N matrix where entry i,j is the total tax
    # paid globally when sending payment to j
    # WITHOUT STRATEGIC BEHAVIOR
    def totalTax(self):
        N = len(list(self.country2StartIndex))
        self.totalTax = np.full((N,N),0.0)
        for source in range(0,N):
            for target in range(0,N):
                # find path from source to target
                i = source*6
                j = target*6+4
                distance = self.distance[i][i+4]
                + self.distance[i+4][j-4+1]
                + self.distance[j-4+1][j]
                self.totalTax[source][target] = 1-np.exp(-distance)

    def averageInboundDividends(self):
        N = len(list(self.country2StartIndex.keys()))
        average_inbound = [0]*N
        for dst in range(1,N+1):
            mean = 0
            end_node = (dst-1)*6 + 4
            for src in range(1,N+1):
                start_node = (src-1)*6
                if self.doubleTaxationRelief[dst] != 'crd':
                    if self.cit[src] < 0.15 and self.eu[src] != 1 and self.eu[dst] ==1:
                        # tax incoming dividends
                        mean += self.cit[dst]
                    else:
                        mean += self.xmp[dst]*self.cit[dst]
                else:
                    #compute tax at source
                    tBA = 1-np.exp(-self.distance[start_node+4][end_node-3])
                    tB = 1-np.exp(-self.distance[start_node][start_node+4])
                    mean += max(0,(self.cit[dst]-(1-tB)*tBA - tB)/((1-tBA)*(1-tB)))
            average_inbound[dst-1] = mean/N
        return average_inbound
