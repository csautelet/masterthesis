# Author: Caroline Sautelet
# Date: 20-02-2017
########################################################
# IndirectUncapacitatedTaxNetwork Class:
# STRATEGIC BEHAVIOR
# NO Anti-Avoidance RULE
########################################################
import networkx as nx
import numpy as np
import math
import DirectTaxNetwork as DTN

class IndirectUncapacitatedTaxNetwork(DTN.DirectTaxNetwork):
    # INPUT: same as DirectTaxNetwork
    def __init__(self,INPUT_FILE,whtI,whtD,whtR,PB):
        DTN.DirectTaxNetwork.__init__(self,INPUT_FILE,whtI,whtD,whtR,PB)
        self.shortestPath()
        self.StrategicTotalTax()
        self.StrategicTaxAtSource()
        #self.nodeBetweenness()
        self.EBEPS_source()
        self.EBEPS()
    # Compute the shortest Path between each pair of countries
    # Using Floyd Algorithm
    def shortestPath(self):
        distance = np.array(self.distance.copy())
        n_nodes = len(self.node2Country)
        # predecessors matrix
        P = np.full((n_nodes,n_nodes),0,dtype = int)
        # keep nbr of nodes in path
        n_in_path = np.full((n_nodes,n_nodes),1, dtype = int)
        # Initialize predecessor matrix
        for u in range(0,n_nodes):
            for v in range(0,n_nodes):
                if u != v and distance[u][v] != math.inf :
                    P[u][v] = u+1
        # Floyd
        for l in range(0,n_nodes):
            for u in range(0,n_nodes):
                for v in range(0,n_nodes):
                    new_distance = distance[u][l] +  distance[l][v]
                    # if smaller distance
                    if distance[u][v] > new_distance:
                        distance[u][v] = new_distance
                        P[u][v] = P[l][v]
                        n_in_path[u][v] = n_in_path[u][l] + n_in_path[l][v]
                    # if equal distance but fewer countries in path
                    elif distance[u][v] == new_distance and n_in_path[u][v] > (n_in_path[u][l] + n_in_path[l][v]):
                        P[u][v] = P[l][v]
                        n_in_path[u][v] = n_in_path[u][l] + n_in_path[l][v]


        self.shortestDistance = distance
        self.path = P
        N = int(n_nodes/6)
        self.countriesPairwiseCost = np.full((N,N),0.0)
        # from distance to effective tax rate
        for i in range(0,N):
            for j in range(0,N):
                src_node = i*6
                dst_node = j*6+4
                self.countriesPairwiseCost[i][j] = 1-np.exp(-self.shortestDistance[src_node][dst_node])
    # INPUT:
    # - src: ID of source country (from 1 to N)
    # - dst: ID of destination country (from 1 to N)
    # OUTPUT: dictionnary
    # - 'graph': netwrokx graph representing optimal path from src to dst
    # - 'DirectPath': list of dictionnaries, NO strategic BEHAVIOR
    #                1 dictionnary per country
    #         key-value pairs: ('Country', Country Name),
    #                           ('Value',effective tax rate in this coutnry)
    # - 'OptimizedPath': list of dictionnaries, strategic BEHAVIOR
    #                1 dictionnary per country
    #         key-value pairs: ('Country', Country Name),
    #                           ('Value',effective tax rate in this coutnry)
    # - 'EBEPS': BEPS in global tax liability (between 0 and 1)
    # - 'EBEPS_source': BEPS in source country (between 0 and 1)
    def SPgraph(self,src,dst):


        # Initialize
        OptimizedPath = dict()
        DirectPath = dict()
        g = nx.DiGraph()
        labels = dict()

        # list of possible colors for nodes
        c = ['r','b','g','y','c','m','w','k']

        # Compute Direct Path cost
        if self.country2Name[src-1] == "Bermuda":
            DirectPath[self.country2Name[src-1]] = 0
            DirectPath[self.country2Name[dst-1]] = self.totalTax[src-1][dst-1]
        else:
            DirectPath[self.country2Name[src-1]] = self.taxAtSource[src-1][dst-1]
            tmp = self.distance[(dst-1)*6+1][(dst-1)*6+4]
            DirectPath[self.country2Name[dst-1]] = 1 - np.exp(-tmp)


        i = (src-1)*6+1 # source node
        j = (dst-1)*6+4+1 # target node
        path = [j] # initialize path

        # while there is an intermediate node
        while self.path[i-1][j-1] != i:
            # add intermediate node in the path
            path = [self.path[i-1][j-1]]+ path
            j = self.path[i-1][j-1]
        # conclude path with starting node
        path = [i] + path

        L = len(path) # number of nodes in the path

        for i in range(1,L+1): #populate the networkx graph
            # one color per country
            thisColor = self.country2StartIndex[self.node2Country[path[i-1]]]//6 +1
            # construct label of the node
            string = self.node2Country[path[i-1]]+ " " +self.node2Vehicle[path[i-1]]+ " "+self.node2Way[path[i-1]]
            g.add_node(i,label = string,color = thisColor)
        L = range(1,L+1)

        #colors = []
        #for i in range(0,int(len(path)/2)):
        #    colors = colors + [c[i]]*2
        for i in L:
            node = path[i-1]
            #labels[i] = self.node2Country[node] + " \n"+ self.node2Vehicle[node] + " \n"+ self.node2Way[node]
            if self.node2Country[node] not in DirectPath.keys():
                DirectPath[self.node2Country[node]] = 0
        # compute tax paid in each country with optimized path
        for i in range(0,int(len(path)/2)):
            a = i*2
            b = i*2+1
            cost = 1-np.exp(-self.distance[path[a]-1][path[b]-1])
            OptimizedPath[self.node2Country[path[a]]] = cost
            g.add_edges_from([(L[a],L[b])],weight = cost)
            if i > 0:
                cost = 1-np.exp(-self.distance[path[a-1]-1][path[a]-1])
                OptimizedPath[self.node2Country[path[a-1]]] += cost
                g.add_edges_from([(L[a-1],L[a])],weight = cost)

        A = []
        for key in DirectPath.keys():
            A += [{'country':key,'value':DirectPath[key]}]
        B = []
        for key in OptimizedPath.keys():
            B += [{'country':key,'value':OptimizedPath[key]}]
        EBEPS = self.EBEPS[(src-1)][(dst-1)]
        EBEPS_source = self.EBEPS_source[(src-1)][(dst-1)]
        return {'graph':g,'DirectPath':A,'OptimizedPath':B,'EBEPS':EBEPS,'EBEPS_source':EBEPS_source}

    def bestStorageCountry(self,src):
        N = len(list(self.country2StartIndex.keys()))
        results = [0]*N
        length = [0]*N
        i = self.country2StartIndex[self.country2Name[src-1]] - 1
        # what if stays in source country
        results[src-1] = 1-np.exp(-self.distance[i][i+4])
        # length of the path
        length[src-1] = 2
        # for each possible storage coutnry
        for country in range(0,N):
            if country != src-1:
                i = (src-1)*6+1
                j = (country)*6+4+1
                # compute optimal path to this country
                path = [j]
                while self.path[i-1][j-1] != i:
                    path = [self.path[i-1][j-1]]+ path
                    j = self.path[i-1][j-1]
                path = [i] + path
                L = len(path)
                length[country] = L
                #compute total cost for storage in this country
                cost = 0
                for l in range(0,L-1):
                    cost += self.distance[path[l]-1][path[l+1]-1]
                results[country] = 1 - np.exp(-cost)
        # Find best storage country
        index = 0
        best = results[0]
        l = length[0]
        for i in range(1,len(results)):
            if results[i] < best or (results[i] == best and length[i] < l):
                index = i
                best = results[i]
                l = length[i]
        print("The winner is : " + str(self.country2Name[index]) + " with a cost of: " + str(results[index]))
        return{'Country': self.country2Name[index], 'cost':results[index]}

    # Compute the betweenness of each node in the graph
    # by computing ALL possible shortest paths!
    def nodeBetweenness(self):
        n_spaths = dict.fromkeys(self.graph, 0.0)
        N = len(list(self.country2StartIndex.keys()))
        for i in range(0,N):
            for j in range(0,N):
                if i == j:
                    continue
                source = i*6 +1
                target = j*6+5
                tmp = self.graph.copy()

                # no return to source country

                for k in range(0,N):
                    if k != i:
                        toremove = [(k*6+4,i*6+1),(k*6+5,i*6+2),(k*6+6,i*6+3)]
                        tmp.remove_edge(k*6+4,i*6+1)
                        tmp.remove_edge(k*6+5,i*6+2)
                        tmp.remove_edge(k*6+6,i*6+3)
                        toremove2 = [(k*6+1,k*6+6),(k*6+2,k*6+6),(k*6+3,k*6+6)]
                        tmp.remove_edges_from(toremove2)


                generator = nx.all_shortest_paths(tmp, source, target,weight = 'weight')
                indexMemory = dict()
                count = 0
                L = 1000
                # Only keep paths having smallest length
                for path in generator:
                    if len(path) < L:
                        #shortest path so far
                        L = len(path)
                        indexMemory = dict()
                        count = 1
                        indexMemory[count] = path
                    elif len(path) == L:
                        count += 1
                        indexMemory[count] = path
                # count is the number of optimal paths
                # for each path having optimal length & cost
                for key in indexMemory:
                    p = indexMemory[key]
                    n = len(p)
                    for k in range(1,n-1):
                        n_spaths[p[k]] += 1/count
        self.nodeBetweenness = n_spaths

    def averageInboundTaxRate(self):
        N = len(list(self.country2StartIndex.keys()))
        averageInboundTaxRate = [0.0]*N
        for i in range(0,N):
            averageInboundTaxRate[i] = sum(self.countriesPairwiseCost[range(0,N)][i])/N
        return averageInboundTaxRate

    def averageOutboundTaxRate(self):
        N = len(list(self.country2StartIndex.keys()))
        averageOutboundTaxRate = [0.0]*N
        for i in range(0,N):
            averageOutboundTaxRate[i] = sum(self.countriesPairwiseCost[i][range(0,N)])/N
        return averageOutboundTaxRate
    # INPUT :
    #   - src : ID of source country (1 to N)
    #   - dst: ID of destination country (1 to N)
    # OUTPUT:
    # - s: string summarizing the shortest path
    def displayShortestPath(self,src,dst):
        i = (src-1)*6 +1
        j = (dst-1)*6+4 +1
        s = " "
        while self.path[i-1][j-1] != i:
            s = s  + self.node2Country[j] + " " + self.node2Vehicle[j] + " " + self.node2Way[j] + "<br />"
            j = self.path[i-1][j-1]
        s = s  + self.node2Country[j] + " " + self.node2Vehicle[j] + " " + self.node2Way[j]+ "<br />"
        s = s  + self.node2Country[i] + " " + self.node2Vehicle[i] + " " + self.node2Way[i] + "<br />"
        return(s)
    # Compute the tax paid in the source country under the
    # Strategic behavior
    def StrategicTaxAtSource(self):
        N = len(list(self.country2StartIndex))

        self.StrategicTaxAtSource = np.full((N,N),0.0)

        for source in range(0,N):
            for target in range(0,N):
                # find path from source to target
                i = source*6
                j = target*6+4
                path = []

                while self.path[i][j] != (i+1):
                    path = [self.path[i][j]-1] + path
                    j = self.path[i][j] - 1
                path = [i] + path
                if len(path) >=3:
                    distance = self.distance[path[0]][path[1]] + self.distance[path[1]][path[2]]
                elif len(path) == 1: #direct route
                    distance = self.distance[i][j]
                self.StrategicTaxAtSource[source][target] = 1-np.exp(-distance)
    # Compute the global tax paid under the Strategic behavior
    def StrategicTotalTax(self):
        self.StrategicTotalTax = self.countriesPairwiseCost.copy()

    # Compute Effective BEPS in total tax liability
    # for each pair of countries
    def EBEPS(self):
        N = len(list(self.country2StartIndex.keys()))
        EBEPS = np.full((N,N),0.0)
        for source in range(0,N):
            for target in range(0,N):
                tau = self.totalTax[source][target]
                if tau != 0:
                    EBEPS[source][target] = (tau-
                    self.StrategicTotalTax[source][target])/tau
        self.EBEPS = EBEPS
    # Compute Effective BEPS at source
    # for each pair of countries
    def EBEPS_source(self):
        N = len(list(self.country2StartIndex.keys()))
        EBEPS_source = np.full((N,N),0.0)
        for source in range(0,N):
            for target in range(0,N):
                tau = self.taxAtSource[source][target]
                if tau != 0:
                    EBEPS_source[source][target] = (tau - self.StrategicTaxAtSource[source][target])/tau
        self.EBEPS_source = EBEPS_source
