from flask import Flask
import flask
app = Flask(__name__)
import IndirectUncapacitatedTaxNetwork as IUTN
import IndirectCapacitatedTaxNetwork as ICTN
import DirectTaxNetwork as D
import networkx as nx
from networkx.readwrite import json_graph
import json
import csv
import numpy as np
import math

@app.route("/graph")  # consider to use more elegant URL in your JS
def IUTN_graph():
    src = flask.request.args['src']
    dst = flask.request.args['dst']
    pb = flask.request.args['pb']
    aa = flask.request.args['aa']
    g30 = flask.request.args['g30']

    if int(aa) == 0:
        if int(pb)==1:
            if EU[src] ==1 and EU[dst]==1:
                res = networkPBEU.SPgraph(int(src),int(dst))
            else:
                res = networkPB.SPgraph(int(src),int(dst))
        else:
            if EU[src]==1 and EU[dst]==1:
                res = networkNoPBEU.SPgraph(int(src),int(dst))
            else:
                res = networkNoPB.SPgraph(int(src),int(dst))
        data = json_graph.node_link_data(res['graph'])
        toSend = {'graph':data,'direct':res['DirectPath'],'optimized':res['OptimizedPath'],'EBEPS':str(round(res['EBEPS']*100,2)),'EBEPS_source':str(round(res['EBEPS_source']*100,2))}#,'strat':res['storage'],'noStrat':res['noStrat']}
        s = json.dumps(toSend)
    else:
        if int(pb)==1:
            if int(g30)==1:
                if EU[src] ==1 and EU[dst]==1:
                    res = AAnetworkPB30EU.SPgraph(int(src),int(dst))
                else:
                    res = AAnetworkPB30.SPgraph(int(src),int(dst))
            else:
                if EU[src] ==1 and EU[dst]==1:
                    res = AAnetworkPB30EU.EscapeGraph(int(src))
                else:
                    res = AAnetworkPB30.EscapeGraph(int(src))
        else:
            if int(g30)==1:
                if EU[src] ==1 and EU[dst]==1:
                    res = AAnetworkNoPB30EU.SPgraph(int(src),int(dst))
                else:
                    res = AAnetworkNoPB30.SPgraph(int(src),int(dst))
            else:
                if EU[src] ==1 and EU[dst]==1:
                    res = AAnetworkNoPB30EU.EscapeGraph(int(src))
                else:
                    res = AAnetworkNoPB30.EscapeGraph(int(src))
        data = json_graph.node_link_data(res['graph'])
        print('EBEPS')
        print(res['EBEPS'])
        print('EBEPS_source')
        print(res['EBEPS_source'])
        toSend = {'graph':data,'direct':res['DirectPath'],'optimized':res['OptimizedPath'],'EBEPS':str(round(res['EBEPS']*100,2)),'EBEPS_source':str(round(res['EBEPS_source']*100,2)),'strat':res['storage'],'noStrat':res['noStrat']}
        s = json.dumps(toSend)
        print(s)
    return (s)
@app.route("/path")  # consider to use more elegant URL in your JS
def IUTN_path():
    src = flask.request.args['src']
    dst = flask.request.args['dst']
    pb = flask.request.args['pb']

    #if int(pb)==1:
    #    print("in pb")
    #    res = networkPB.displayShortestPath(int(src),int(dst))
    #else:
    #    res = networkNoPB.displayShortestPath(int(src),int(dst))

    #return (res)
@app.route("/EBEPS")
def IUTN_EBEPS():
    src = flask.request.args['src']
    dst = flask.request.args['dst']
    pb = flask.request.args['pb']

    if int(pb)==1:
        print("in pb")
        res = networkPB.EBEPS[int(src)-1][int(dst)-1]
    else:
        res = networkNoPB.EBEPS[int(src)-1][int(dst)-1]
    return (str(round(res*100,2)))
@app.route("/EBEPS_source")
def IUTN_EBEPS_source():
    src = flask.request.args['src']
    dst = flask.request.args['dst']
    pb = flask.request.args['pb']

    if int(pb)==1:
        print("in pb")
        res = networkPB.EBEPS_source[int(src)-1][int(dst)-1]
    else:
        res = networkNoPB.EBEPS_source[int(src)-1][int(dst)-1]
    return (str(round(res*100,2)))

if __name__ == "__main__":
    EU = dict()
    with open('data/EU.csv', 'r') as f:
        for line in f:
            s = line.split(',')
            EU[s[0]] = int(s[1])

    networkPBEU = IUTN.IndirectUncapacitatedTaxNetwork('data/dataEU.csv','data/whtIEU.csv','data/whtDEU.csv','data/whtREU.csv',1)
    networkNoPBEU = IUTN.IndirectUncapacitatedTaxNetwork('data/dataEU.csv','data/whtIEU.csv','data/whtDEU.csv','data/whtREU.csv',0)
    AAnetworkPB30EU = ICTN.IndirectCapacitatedTaxNetwork('data/dataEU.csv',0.06,'data/AAR_all_GermanyEU_new.csv','data/whtIEU.csv','data/whtDEU.csv','data/whtREU.csv',1)
    AAnetworkPB30EU.sensitivity_interest()

    networkPB = IUTN.IndirectUncapacitatedTaxNetwork('data/data.csv','data/whtI.csv','data/whtD.csv','data/whtR.csv',1)
    networkNoPB = IUTN.IndirectUncapacitatedTaxNetwork('data/data.csv','data/whtI.csv','data/whtD.csv','data/whtR.csv',0)
    AAnetworkPB30 = ICTN.IndirectCapacitatedTaxNetwork('data/data.csv',0.06,'data/AAR_all_Germany_new.csv','data/whtI.csv','data/whtD.csv','data/whtR.csv',1)
    AAnetworkNoPB30 = ICTN.IndirectCapacitatedTaxNetwork('data/data.csv',0.06,'data/AAR_all_Germany_new.csv','data/whtI.csv','data/whtD.csv','data/whtR.csv',0)

    app.run()
