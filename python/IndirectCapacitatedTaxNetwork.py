# Author: Caroline Sautelet
# Date: 02-03-2017
########################################################
# IndirectCapacitatedTaxNetwork Class:
# STRATEGIC BEHAVIOR
# Anti-Avoidance RULE
########################################################
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cmx
import matplotlib.colors as colors
from gurobipy import *
import networkx as nx
import math
import DirectTaxNetwork as DTN
import IndirectUncapacitatedTaxNetwork as IUTN

def helper_entry_nodes(n_nodes,a,c_len,vehicle,ways,i_vehicle,g,flow,i_country,node_identification,info_node,flow_per_edge):
    bool_i_in = 0
    for j in range(0,3):
        if flow[i_country*c_len+j+i_vehicle*3] !=0:
            if bool_i_in == 0:
                s = a.country2Name[i_country] + " " + vehicle[i_vehicle] + " " + ways[0]
                info_node[n_nodes] = {'country':i_country,'vehicle':vehicle[i_vehicle],'way':ways[0]}
                g.add_node(n_nodes,label = s,color = i_country+1)
                node_identification[(i_country,i_vehicle,0)] = n_nodes
                index_in = n_nodes
                n_nodes += 1
                bool_i_in += 1
            if (i_country,j,1) not in node_identification:
                s = a.country2Name[i_country] + " "+ vehicle[j] + " "+ ways[1]
                info_node[n_nodes] = {'country':i_country,'vehicle':vehicle[j],'way':ways[1]}
                g.add_node(n_nodes,label = s,color = i_country+1)
                node_identification[(i_country,j,1)] = n_nodes
                n_nodes += 1
            w = node_identification[(i_country,i_vehicle,0)]
            z = node_identification[(i_country,j,1)]
            flow_per_edge[(w,z)] = flow[i_country*c_len+j+i_vehicle*3]
            g.add_edges_from([(w,z)],weight = round(flow[i_country*c_len+j+i_vehicle*3],5))
            print("weight helper = " + str(round(flow[i_country*c_len+j+i_vehicle*3],5)))

    return n_nodes

class IndirectCapacitatedTaxNetwork(IUTN.IndirectUncapacitatedTaxNetwork):
    flow = dict()
    # INPUT:
    # - INPUT_FILE, whtI, whtD, whtR, PB: same as DirectTaxNetwork
    # - limitRoyalties: percentage allowed to leave source country as royalties
    # - AAR: csv, sep = , one line per country
    # Country Name, percentage allowed to leave as interests
    def __init__(self,INPUT_FILE,limitRoyalties,AAR,whtI,whtD,whtR,PB,):
        IUTN.IndirectUncapacitatedTaxNetwork.__init__(self,INPUT_FILE,whtI,whtD,whtR,PB)
        self.limitRoyalties = limitRoyalties
        self.limitInterests = dict()
        #self.equity = dict()
        with open(AAR, 'r', encoding='latin-1') as f:
            for line in f:
                s = line.split(",")
                i = self.country2StartIndex[s[0]]
                i = i//6 +1
                self.limitInterests[i] = float(s[1])
                #self.equity[i] = float(s[2])

    # Compute shortest Path fulfilling AAR requirements
    # for all pair of countries
    def all_shortestPath(self):
        n_nodes = len(list(self.node2Way.keys()))
        N = int(n_nodes/6)
        self.OptimalCost = np.full((N,N),0.0)
        self.buildObjective(tradeoff)

        for source in range(1,N+1):
            for target in range(1,N+1):
                if source == target:
                    self.OptimalCost[source-1][target-1] = self.distance[(source-1)*6][(target-1)*6+4]
                    continue

                self.buildInequalityMatrix(source)
                self.buildEqualityMatrix(source,target)
                A = np.concatenate((self.EqualityMatrix,self.InequalityMatrix),axis= 0)
                n_ub = self.InequalityMatrix.shape
                n_ub = n_ub[0]
                n_eq = self.EqualityMatrix.shape
                n_var = n_eq[1]
                n_eq = n_eq[0]
                c = self.objective
                #c = [a + b for a, b in zip(self.objective, [0]*n_var)]
                sense =  [GRB.EQUAL]*n_eq +[GRB.GREATER_EQUAL]*(N+1) + [GRB.LESS_EQUAL]*N
                rhs = [0]*(n_eq + N+1)+ [1]*N
                lb = [0]*n_var
                ub = [1]*n_var
                vtype = [GRB.CONTINUOUS]*n_var
                sol = [0]*n_var
                rhs[(source-1)*6] = -1
                rhs[(target-1)*6+4] = 1
                success = self.dense_optimize(n_ub+n_eq, n_var, c, A, sense, rhs, lb, ub, vtype, sol)
                if success:
                    tmp = 0
                    for i in range(0,n_var):
                        if sol[i] != 0:
                            tmp += sol[i]*(self.objective[i] - tradeoff)
                    self.OptimalCost[source-1][target-1] = 1 - np.exp(-tmp)
                    self.flow[(source,target)] = sol
                else:
                    self.OptimalCost[source-1][target-1] = 10

    def summary(self,filename):
        N = len(list(self.country2StartIndex.keys()))
        rate = np.zeros((N,N))
        tradeoff = 0.00001
        for i in range(1,N+1):
            for j in range(1,N+1):
                res = self.AAR_shortestPath(i,j)
                rate[i-1][j-1] = res['cost']
        np.savetxt(filename,rate,delimiter = ',')

    def sensitivity_interest(self):
        src = 1
        dst = 3

        # limitInterests goes from 0 to 100% by percent
        L = len(self.limitInterests)
        li = range(0,101)
        li = [i/100 for i in li]
        N = len(li)
        optimalCost = [0]*N
        k = 0
        for i in li:
            for key in self.limitInterests.keys():
                 self.limitInterests[key] = i
            print(i)
            res = self.AAR_shortestPath(src,dst)
            optimalCost[k] = res['cost']*100
            k+=1
        li = [i*100 for i in li]
        plt.plot(li, optimalCost, 'r-')
        plt.xlim(0, 100)
        plt.ylim(0, 40)
        plt.xlabel('Deductible interests (%)')
        plt.ylabel('Optimal Tax Rate (%)')
        plt.title('Sensitivity analysis: Deductibility of Interests')
        plt.show()
    def sensitivity_royalties(self):
        src = 1
        dst = 3
        for key in self.limitInterests.keys():
             self.limitInterests[key] = 0.3
        # limitInterests goes from 0 to 100% by percent
        lr = range(0,101)
        lr = [i/100 for i in lr]
        N = len(lr)
        optimalCost = [0]*N
        k = 0
        for i in lr:
            self.limitRoyalties = i
            print(i)
            res = self.AAR_shortestPath(src,dst)
            optimalCost[k] = res['cost']*100
            k+=1
        lr = [i*100 for i in lr]
        plt.plot(lr, optimalCost, 'g-')
        plt.xlim(0, 100)
        plt.ylim(0, 40)
        plt.xlabel('Deductible royalties (%)')
        plt.ylabel('Optimal Tax Rate (%)')
        plt.title('Sensitivity analysis: Deductibility of Royalties')
        plt.show()
    # Compute shortest path from src to dst fulfilling
    # Anti-Avoidance requirements
    # INPUT:
    # - src: ID of source country (1 to N)
    # - dst: ID of destination country (1 to N)
    # OUTPUT:
    # - res : dictionnary
    #         - 'cost': optimal tax rate
    #         - 'flow': optimal flows
    def AAR_shortestPath(self,src,dst):

        res = dict()
        N = len(list(self.country2StartIndex.keys()))

        c_len = 9+3*N
        ######################################################################
        # First optimization PROBLEM
        # doesn't take care of nbr of visited countries
        ######################################################################

        self.buildObjective()
        self.buildInequalityMatrix(src)
        self.buildEqualityMatrix(src,dst)
        # build global matrix of constraints
        A = np.concatenate((self.EqualityMatrix,self.InequalityMatrix),axis= 0)
        n_ub = self.InequalityMatrix.shape
        n_ub = n_ub[0] # nbr of inequalities constraints
        n_eq = self.EqualityMatrix.shape
        n_var = n_eq[1] # nbr of variables
        n_eq = n_eq[0] # nbr of equalities constraints
        sense =  [GRB.EQUAL]*n_eq +[GRB.LESS_EQUAL]*(N+1) + [GRB.LESS_EQUAL]*N + [GRB.GREATER_EQUAL]*3*N*N
        # right hand side of constraints
        rhs = [0]*(n_eq + N+1) +[1]*N + [0]*3*N*N
        rhs[(src-1)*6] = -1
        rhs[(dst-1)*6+4] = 1

        lb = [0]*n_var # lower bound for each flow
        ub = [100]*n_var # upper bound for each florw
        vtype = [GRB.CONTINUOUS]*n_var

        # allocate memory for solution
        sol = [0]*n_var
        print("Number of equalities = " + str(n_eq))
        print("batch 1 = "  +str(N+1))
        print("Batch 2 = " + str(N))
        print("Batch 3 = " + str(3*N*N))

        # Run GUROBI solver
        success = self.dense_optimize(n_ub+n_eq, n_var, self.objective, A, sense, rhs, lb, ub, vtype, sol)
        # get optimal tax rate
        res['cost'] = sol[N*c_len+3*N+3*N]
        # display all non zero flows
        for i in range(0,N*c_len+3*N+3*N+1):
             if sol[i] != 0:
                 print("Flow # " + str(i) + " = " + str(sol[i]))
        # display optimal tax rate
        print("Try to be equal to " + str(sol[N*c_len+3*N+3*N]))
        ######################################################################
        # SECOND optimization PROBLEM
        # keep tax rate at Optimal value
        # Minimize number of visited countries
        # L0 norm approximated by L1 norm
        ######################################################################
        # add constraint according to which tax paid must be equal
        # to optimal solution found above
        tmp = np.zeros((1,N*c_len+3*N+3*N+1))
        tmp[0][N*c_len+3*N+3*N] = 1
        NEW = np.concatenate((tmp,A),axis = 0)
        sense =  [GRB.EQUAL]*(n_eq+1) +[GRB.LESS_EQUAL]*(N+1) + [GRB.LESS_EQUAL]*N + [GRB.GREATER_EQUAL]*3*N*N#[GRB.GREATER_EQUAL]*(N+1)

        # right hand side of constraints
        rhs = [sol[N*c_len+3*N+3*N]] + [0]*(n_eq + N+1) +[1]*N + [0]*3*N*N
        rhs[(src-1)*6+1] = -1
        rhs[(dst-1)*6+4+1] = 1

        lb = [0]*n_var
        ub = [100]*n_var
        for i in range(0,N):
            ub[i*c_len+8] = 0.06
        vtype = [GRB.CONTINUOUS]*n_var
        sol = [0]*n_var

        # Build objective function
        obj = np.zeros((1,N*c_len+3*N+3*N+1))+1
        obj = obj[0]

        # RUN GUROBI SOLVER
        success = self.dense_optimize(n_ub+n_eq+1, n_var, obj, NEW, sense, rhs, lb, ub, vtype, sol)

        # display non zero flows
        for i in range(0,N*c_len+3*N+3*N+1):
            if sol[i] != 0:
                print("Flow # " + str(i) + " = " + str(sol[i]))

        res['flow'] = sol
        return res

    def SPgraph(self,src,dst):
        res = self.AAR_shortestPath(src,dst)
        sol = res['flow']
        print("optimalCost  is now for this i " + str(res['cost']))

        n_nodes = len(list(self.node2Way.keys()))
        N = int(n_nodes/6)
        c_len = 9+3*N # number of flows per country

        N = len(list(self.country2StartIndex.keys()))
        DirectPath = dict()
        DirectPath[self.country2Name[src-1]] = self.taxAtSource[src-1][dst-1]
        sofar = DirectPath[self.country2Name[src-1]]
        tmp = self.distance[(dst-1)*6+1][(dst-1)*6+4]
        DirectPath[self.country2Name[dst-1]] = (1 - np.exp(-tmp))*(1-sofar)
        storage = dict()
        noStrat = dict()
        noStrat[self.country2Name[dst-1]] = 1 - sofar - (1 - np.exp(-tmp))*(1-sofar)
        storage[self.country2Name[dst-1]] = sol[(dst-1)*c_len + 4] + sol[(dst-1)*c_len + 1] + sol[(dst-1)*c_len + 7]
        print("STORAGE !!!")
        print(storage[self.country2Name[dst-1]])
        OptimizedPath = dict()
        # compute optimized path tax per country
        for country in range(0,N):
            sumOfFlows = sol[country*c_len]+sol[country*c_len+1]+sol[country*c_len+2]+sol[country*c_len+3] + sol[country*c_len+4]+sol[country*c_len+5]+sol[country*c_len+6]+sol[country*c_len+7]+sol[country*c_len+8]
            # entry nodes of country to taxes
            entry = sol[N*c_len+country*3]+ sol[N*c_len+country*3 + 1]+ sol[N*c_len+country*3 + 2]
            # exit nodes of country to taxes
            exit = sol[N*c_len+3*N+country*3]+ sol[N*c_len+3*N+country*3 + 1]+ sol[N*c_len+3*N+country*3 + 2]
            if entry + exit != 0:
                country_name = self.country2Name[country]
                OptimizedPath[country_name]  = entry + exit
            elif sumOfFlows != 0:
                country_name = self.country2Name[country]
                OptimizedPath[country_name] = 0
        for key in DirectPath.keys():
            if key not in OptimizedPath.keys():
                OptimizedPath[key] = 0
        for key in OptimizedPath.keys():
            if key not in DirectPath.keys():
                DirectPath[key] = 0
        if self.country2Name[dst-1] == "United States":
            #crediting method
            upstream = 0
            for key in OptimizedPath.keys():
                if key != "United States":
                    upstream += OptimizedPath[key]
            OptimizedPath["United States"] = max(0,0.35-upstream)

        a = self
        g = nx.DiGraph()
        vehicle = ['interest','dividend','royalties']
        ways = ['in','out']
        labels = {}
        n_nodes = 0
        node_identification = dict()
        info_node = dict()
        flow_per_edge = dict()
        # for each country
        for i in range(0,N):
            # intra jurisdictional edges
            n_nodes = helper_entry_nodes(n_nodes,a,c_len,vehicle,ways,0,g,sol,i,node_identification,info_node,flow_per_edge)
            n_nodes = helper_entry_nodes(n_nodes,a,c_len,vehicle,ways,1,g,sol,i,node_identification,info_node,flow_per_edge)
            n_nodes = helper_entry_nodes(n_nodes,a,c_len,vehicle,ways,2,g,sol,i,node_identification,info_node,flow_per_edge)

        for i in range(0,N):
            # inter jurisdictional edges
            for j in range(0,3):
                offset = j*N
                for k in range(offset,offset+N):
                    if sol[i*c_len+9+k] != 0.0:
                        # add edges between country i and k-offset for vehicle j
                        w = node_identification[(i,j,1)]
                        z = node_identification[(k-offset,j,0)]
                        flow_per_edge[(w,z)] = sol[i*c_len+9+k]
                        g.add_edges_from([(w,z)],weight = round(sol[i*c_len+9+k],5))
                        print("weight = " +str(round(sol[i*c_len+9+k],5)))

        print(info_node)
        nodes = g.nodes()
        for i in nodes:
            current = info_node[i]
            if current['country']+1 == src and current['way'] == 'in' and current['vehicle'] == 'interest':
                a = i
            if current['country'] + 1 == dst and current['way'] == 'out' and current['vehicle'] == 'dividend':
                b=i
        print(flow_per_edge)

        #for path in nx.all_simple_paths(g, source= a, target=b):
        #    self.compute_tax_per_country(path,info_node,tax_per_country,flow_per_edge)
        #for country in tax_per_country.keys():
        #    if country not in DirectPath.keys():
        #        DirectPath[country] = 0
        A = []
        for key in DirectPath.keys():
            A += [{'country':key,'value':DirectPath[key]}]
        B = []
        C = []
        D = []
        for key in OptimizedPath.keys():
            B += [{'country':key,'value':OptimizedPath[key]}]
            if key not in storage.keys():
                C += [{'country':key,'value':0}]
            else:
                C += [{'country':key,'value':storage[key]}]
            if key not in noStrat.keys():
                D += [{'country':key,'value':0}]
            else:
                D += [{'country':key,'value':noStrat[key]}]
        tax_direct = 0
        tax_optimized = 0
        for key in DirectPath.keys():
            tax_direct += DirectPath[key]
            tax_optimized += OptimizedPath[key]
        EBEPS = (tax_direct-tax_optimized)/tax_direct
        #EBEPS at source
        src_country = self.node2Country[(src-1)*6+1]
        EBEPS_source = (DirectPath[src_country] - OptimizedPath[src_country])/DirectPath[src_country]
        return {'graph':g,'DirectPath':A,'OptimizedPath':B,'EBEPS':EBEPS,'EBEPS_source':EBEPS_source,'storage':C,'noStrat':D}

    def EscapeGraph(self,src):
        sol = self.GlobalOpitmizationProblem(src)
        N = len(list(self.country2StartIndex.keys()))
        c_len = 9+3*N

        DirectPath = dict()
        DirectPath[self.country2Name[src-1]] = self.cit[src]
        noStrat = dict()
        noStrat[self.country2Name[src-1]] = 1-self.cit[src]
        OptimizedPath = dict()
        # compute optimized path tax per country
        for country in range(0,N):
            sumOfFlows = sol[country*c_len]+sol[country*c_len+1]+sol[country*c_len+2]+sol[country*c_len+3] + sol[country*c_len+4]+sol[country*c_len+5]+sol[country*c_len+6]+sol[country*c_len+7]+sol[country*c_len+8]
            # entry nodes of country to taxes
            entry = sol[N*c_len+country*3]+ sol[N*c_len+country*3 + 1]+ sol[N*c_len+country*3 + 2]
            # exit nodes of country to taxes
            exit = sol[N*c_len+3*N+country*3]+ sol[N*c_len+3*N+country*3 + 1]+ sol[N*c_len+3*N+country*3 + 2]
            if entry + exit != 0:
                country_name = self.country2Name[country]
                OptimizedPath[country_name]  = entry + exit
            elif sumOfFlows != 0:
                country_name = self.country2Name[country]
                OptimizedPath[country_name] = 0
        for key in DirectPath.keys():
            if key not in OptimizedPath.keys():
                OptimizedPath[key] = 0
        for key in OptimizedPath.keys():
            if key not in DirectPath.keys():
                DirectPath[key] = 0
        a = self
        g = nx.DiGraph()
        vehicle = ['interest','dividend','royalties']
        ways = ['in','out']
        labels = {}
        n_nodes = 0
        node_identification = dict()
        info_node = dict()
        flow_per_edge = dict()
        # for each country
        for i in range(0,N):
            # intra jurisdictional edges
            n_nodes = helper_entry_nodes(n_nodes,a,c_len,vehicle,ways,0,g,sol,i,node_identification,info_node,flow_per_edge)
            n_nodes = helper_entry_nodes(n_nodes,a,c_len,vehicle,ways,1,g,sol,i,node_identification,info_node,flow_per_edge)
            n_nodes = helper_entry_nodes(n_nodes,a,c_len,vehicle,ways,2,g,sol,i,node_identification,info_node,flow_per_edge)

        for i in range(0,N):
            # inter jurisdictional edges
            for j in range(0,3):
                offset = j*N
                for k in range(offset,offset+N):
                    if sol[i*c_len+9+k] != 0.0:
                        # add edges between country i and k-offset for vehicle j
                        w = node_identification[(i,j,1)]
                        z = node_identification[(k-offset,j,0)]
                        flow_per_edge[(w,z)] = sol[i*c_len+9+k]
                        g.add_edges_from([(w,z)],weight = round(sol[i*c_len+9+k],5))
                        print("weight = " +str(round(sol[i*c_len+9+k],5)))

        #print(info_node)
        #nodes = g.nodes()
        #for i in nodes:
        #    current = info_node[i]
        #    if current['country']+1 == src and current['way'] == 'in' and current['vehicle'] == 'interest':
        #        a = i
        #    if current['country'] + 1 == dst and current['way'] == 'out' and current['vehicle'] == 'dividend':
        #        b=i
        print(flow_per_edge)

        #for path in nx.all_simple_paths(g, source= a, target=b):
        #    self.compute_tax_per_country(path,info_node,tax_per_country,flow_per_edge)
        #for country in tax_per_country.keys():
        #    if country not in DirectPath.keys():
        #        DirectPath[country] = 0
        storage = dict()
        for i in range(0,N):
            #check if somehing stored
            if sol[N*c_len+3*N+3*N+i]!=0:
                #something stored!
                storage[self.country2Name[i]] = sol[N*c_len+3*N+3*N+i]


        A = []
        for key in DirectPath.keys():
            A += [{'country':key,'value':DirectPath[key]}]
        B = []
        for key in OptimizedPath.keys():
            if key not in storage.keys():
                storage[key] = 0
            B += [{'country':key,'value':OptimizedPath[key]}]
        C  = []
        D = []
        for key in storage.keys():
            C += [{'country':key,'value':storage[key]}]
            if key not in noStrat.keys():
                D+= [{'country':key,'value':0}]
            else:
                D += [{'country':key,'value':noStrat[key]}]
        tax_direct = 0
        tax_optimized = 0
        for key in DirectPath.keys():
            tax_direct += DirectPath[key]
            tax_optimized += OptimizedPath[key]
        EBEPS = (tax_direct-tax_optimized)/tax_direct
        #EBEPS at source
        src_country = self.node2Country[(src-1)*6+1]
        EBEPS_source = (DirectPath[src_country] - OptimizedPath[src_country])/DirectPath[src_country]
        return {'graph':g,'DirectPath':A,'OptimizedPath':B,'EBEPS':EBEPS,'EBEPS_source':EBEPS_source,'storage':C,'noStrat':D}


    def GlobalOpitmizationProblem(self,src):
        N = len(list(self.country2StartIndex.keys()))
        c_len = 9+3*N
        self.objective = [0]*(N*c_len+3*N+3*N+N+1)
        self.objective[N*c_len+3*N+3*N+N] = 1

        self.GlobalInequalityMatrix(src)
        self.GlobalEqualityMatrix(src)

        A = np.concatenate((self.EqualityMatrix,self.InequalityMatrix),axis= 0)
        n_ub = self.InequalityMatrix.shape
        n_ub = n_ub[0]
        n_eq = self.EqualityMatrix.shape
        n_var = n_eq[1]
        n_eq = n_eq[0]
        print("# equalities : " + str(n_eq))
        print('# less equal 1: ' + str(N+1))
        print('# addes less equal = ' + str(N))
        sense =  [GRB.EQUAL]*n_eq +[GRB.LESS_EQUAL]*(N+1)+ [GRB.LESS_EQUAL]*N + [GRB.GREATER_EQUAL]*3*N*N#[GRB.GREATER_EQUAL]*(N+1)
        rhs = [0]*(n_eq + N+1) +[1]*N + [0]*3*N*N
        lb = [0]*n_var
        ub = [100]*n_var
        vtype = [GRB.CONTINUOUS]*n_var
        sol = [0]*n_var
        rhs[(src-1)*6] = -1
        #rhs[(dst-1)*6+4] = 1
        success = self.dense_optimize(n_ub+n_eq, n_var, self.objective, A, sense, rhs, lb, ub, vtype, sol)
        OptimalCost = sol[N*c_len+3*N+3*N+N]

        # SECOND OPTIMIZATION PROBLEM TO MINIMIZE NBR OF NON ZERO FLOWS
        print("Try to be equal to " + str(OptimalCost))
        tmp = np.zeros((1,N*c_len+3*N+3*N+N+1))
        tmp[0][N*c_len+3*N+3*N+N] = 1
        NEW = np.concatenate((tmp,A),axis = 0)
        sense =  [GRB.EQUAL]*(n_eq+1) +[GRB.LESS_EQUAL]*(N+1)+ [GRB.LESS_EQUAL]*N + [GRB.GREATER_EQUAL]*3*N*N#[GRB.GREATER_EQUAL]*(N+1)
        rhs = [OptimalCost] + [0]*(n_eq + N+1) +[1]*N + [0]*3*N*N
        lb = [0]*n_var
        ub = [100]*n_var
        vtype = [GRB.CONTINUOUS]*n_var
        sol = [0]*n_var
        rhs[(src-1)*6+1] = -1
        #rhs[(dst-1)*6+4] = 1
        obj = np.zeros((1,N*c_len+N +3*N+3*N+1))
        #obj[0][range(0,N*c_len)] = 1
        obj[0][range(8,N*c_len,c_len)] = 1
        obj[0][range(4,N*c_len,c_len)] = 1
        success = self.dense_optimize(n_ub+n_eq+1, n_var, obj[0], NEW, sense, rhs, lb, ub, vtype, sol)


        for i in range(0,len(sol)):
            if sol[i]!=0 and i < N*c_len:
                #who is this flow?
                country_index = i//c_len
                reste = i%c_len
                if reste <= 8:
                    a = reste//3
                    b = reste%3
                    print("Country: " + self.country2Name[country_index] + " In: " + str(a) + ' OUT: ' + str(b) + " flow: " +str(sol[i]))
                else:
                    reste = reste - 9
                    a = reste//N
                    b = reste%N
                    print("FROM " + self.country2Name[country_index] + " to " + self.country2Name[b] + " in " + str(a)+ " flow: " +str(sol[i]))
            elif sol[i]!=0 and i >= N*c_len+3*N+3*N and i != N*c_len+6*N+N:
                print("Reserve flow non zero for country: " + self.country2Name[i-N*c_len-6*N] + " value = "  +str(sol[i]))
        return sol
    def buildEqualityMatrix(self,src,dst):
        n_nodes = len(list(self.node2Way.keys()))
        N = int(n_nodes/6)
        # demand vector
        # allocate memory
        c_len = 9+3*N
        A = np.full((6*N+2*(N-1) + 3*N +1 +1,N*c_len+3*N + 3*N+1),0.0)
        for k in range(0,N):
            i = k
            j = i*6
            # flow conservation for ENTRY NODES
            # interest
            A[j][range(i*c_len,i*c_len+3)] = [-1,-1,-1]
            A[j][range(9+i,N*c_len+9+i,c_len)] = np.full((1,N),1,dtype = int)
            A[j][N*c_len+3*k] = -1
            A[j][i*c_len+9+i] = 0
            # dividend
            A[j+1][range(i*c_len+3,i*c_len+6)] = [-1,-1,-1]
            A[j+1][range(9+N+i,N*c_len+9+N+i,c_len)] = np.full((1,N),1,dtype = int)
            A[j+1][N*c_len+3*k+1] = -1
            A[j+1][i*c_len+N+9+i] = 0
            # royalties
            A[j+2][range(i*c_len+6,i*c_len+9)] = [-1,-1,-1]
            A[j+2][range(9+2*N+i,N*c_len+9+2*N+i,c_len)] = np.full((1,N),1,dtype = int)
            A[j+2][N*c_len+3*k+2] = -1
            A[j+2][i*c_len+2*N+9+i] = 0
            # flow conservation for EXIT NODES
            #interest
            A[j+3][range(i*c_len,i*c_len+7,3)] = [1,1,1]
            A[j+3][range(9+i*c_len,i*c_len+9+N)] = np.full((1,N),-1,dtype = int)
            A[j+3][i*c_len+9+i] = 0
            # dividend
            if k+1 == dst:
                A[j+4][range(i*c_len+1,i*c_len+8,3)] = [1,1,1]
                A[j+4][range(9+i*c_len+N,i*c_len+9+2*N)] = np.full((1,N),-1,dtype = int)
                A[j+4][N*c_len+3*N+3*N] = 1
                A[j+4][i*c_len+9+i+N] = 0
            else:
                A[j+4][range(i*c_len+1,i*c_len+8,3)] = [1,1,1]
                A[j+4][range(9+i*c_len+N,i*c_len+9+2*N)] = np.full((1,N),-1,dtype = int)
                A[j+4][i*c_len+9+i+N] = 0
            # royalties
            A[j+5][range(i*c_len+2,i*c_len+9,3)] = [1,1,1]
            A[j+5][range(9+i*c_len+2*N,i*c_len+9+3*N)] = np.full((1,N),-1,dtype = int)
            A[j+5][i*c_len+9+i+2*N] = 0

        current = 6*N
        for i in range(0,N):# no transfo into royalties except source country = 2(N-1)
            if i+1 !=src:
                A[current][i*c_len + 2] = 1
                A[current+1][i*c_len + 5] = 1
                current += 2
        #add constraint linking entry to taxes node! = 3N
        for country in range(0,N):
            #interests
            A[current][country*c_len + 1] = -self.cit[country+1]
            A[current][N*c_len + country*3] = (1-self.cit[country+1])
            #dividends
            A[current+1][country*c_len + 3+ 1] = -self.cit[country+1]*self.xmp[country+1]
            A[current+1][N*c_len + country*3 + 1]= 1-self.cit[country+1]*self.xmp[country+1]
            #royalties
            A[current+2][country*c_len + 6+ 1] = -self.cit[country+1]*self.pb[country+1]
            A[current+2][N*c_len + country*3 +2] = 1-self.cit[country+1]*self.pb[country+1]
            current += 3

        # conservation taxes node! = 1
        A[current][range(N*c_len,N*c_len+3*N)] = [1]*3*N
        A[current][range(N*c_len+3*N,N*c_len+3*N+3*N)] = [1]*3*N
        A[current][N*c_len+3*N+3*N] = -1

        # nothing leaves destination
        A[current+1][range((dst-1)*c_len + 9, dst*c_len)] = [1]*3*N

        self.EqualityMatrix = A
    def buildInequalityMatrix(self,src):
        n_nodes = len(list(self.node2Way.keys()))
        N = int(n_nodes/6)
        # allocate memory
        c_len = 9+3*N
        A = np.full((N+1+N+3*N*N,N*c_len+3*N + 3*N+1),0.0)
        a = self.limitRoyalties*100
        b = -(100-a)
        A[0][range((src-1)*c_len,(src-1)*c_len+9)] = [-a,-a,-b]*3 #royalties
        A[0][range(N*c_len+3*(src-1),N*c_len+3*(src-1)+3)] = [-a,-a,-a]

        for j in range(1,N+1):#interests
            a = self.limitInterests[j]*100
            b = -(100-a)
            i = j-1
            A[j][range(i*c_len,i*c_len+9)] = [-b,-a,-a]*3
            A[j][range(N*c_len+3*i,N*c_len+3*i+3)] = [-a,-a,-a]
        for k in range(0,N):#no profit > 1 in any country = no loop
            index = N+1+k
            A[index][range(k*c_len,k*c_len+9)] = [1]*9
            A[index][range(N*c_len+k*3,N*c_len+k*3+3)] = [1]*3


        current = index+1
        #add constraint linking exit to taxes node! = 3N
        for country in range(0,N):
            for dst in range(0,N):
                A[current][N*c_len + 3*N +country*3] = (1-self.whtI[country][dst])
                A[current][country*c_len+9+dst] = -self.whtI[country][dst]

                A[current+1][N*c_len + 3*N +country*3+1]= (1-self.whtD[country][dst])
                A[current+1][country*c_len+9+N+dst] = -self.whtD[country][dst]

                A[current+2][N*c_len + 3*N +country*3+2]= (1-self.whtR[country][dst])
                A[current+2][country*c_len+9+2*N+dst] = -self.whtR[country][dst]
                current += 3

        self.InequalityMatrix = A
    def buildObjective(self):
        n_nodes = len(list(self.node2Way.keys()))
        N = int(n_nodes/6)
        c_len = 9+3*N
        tmp = np.zeros((1,N*c_len+3*N+ 3*N+1))
        tmp[0][N*c_len+3*N+3*N] = 1
        self.objective = tmp[0]
    def dense_optimize(self,rows, cols, c, A, sense, rhs, lb, ub, vtype,solution):
      model = Model()
      # Add variables to model
      vars = []
      for j in range(cols):
        vars.append(model.addVar(lb=lb[j], ub=ub[j], vtype=vtype[j]))

      # Populate A matrix
      for i in range(rows):
        expr = LinExpr()
        for j in range(cols):
          if A[i][j] != 0:
            expr += A[i][j]*vars[j]
        if i == 0:
            print(rhs[i])
        model.addConstr(expr, sense[i], rhs[i])

      # Populate objective
      obj = LinExpr()

      for j in range(cols):
        if c[j] != 0:
          obj += c[j]*vars[j]
      model.setObjective(obj)
      model.write('MinimumCostFlow.lp')

      # Solve
      model.optimize()

      cons=model.getConstrs()
      l = len(cons)
      for i in range(0,l):
         if cons[i].getAttr('Pi') != 0 and sense[i] == GRB.LESS_EQUAL:
              print("constraint number: " + str(i))
              print(cons[i].getAttr('Pi'))
              print("constraint sense: "+ str(sense[i]))

      # Write model to a file
      model.write('MinimumCostFlow.lp')

      if model.status == GRB.Status.OPTIMAL:
        x = model.getAttr('x', vars)
        for i in range(cols):
          solution[i] = x[i]
        return True
      else:
        return False
    def GlobalInequalityMatrix(self,src):
        n_nodes = len(list(self.node2Way.keys()))
        N = int(n_nodes/6)
        # allocate memory
        c_len = 9+3*N
        A = np.full((N+1+N+3*N*N,N*c_len+3*N + 3*N+N +1),0.0)

        a = self.limitRoyalties*100
        b = -(100-a)
        A[0][range((src-1)*c_len,(src-1)*c_len+9)] = [-a,-a,-b]*3 #royalties
        A[0][range(N*c_len+3*(src-1),N*c_len+3*(src-1)+3)] = [-a,-a,-a]

        for j in range(1,N+1):#interests
            a = self.limitInterests[j]*100
            b = -(100-a)
            i = j-1
            A[j][range(i*c_len,i*c_len+9)] = [-b,-a,-a]*3
            A[j][range(N*c_len+3*i,N*c_len+3*i+3)] = [-a,-a,-a]
        for k in range(0,N):#no profit > 1 in any country = no loop
            index = N+1+k
            A[index][range(k*c_len,k*c_len+9)] = [1]*9
            A[index][range(N*c_len+k*3,N*c_len+k*3+3)] = [1]*3
            A[index][N*c_len+3*N+3*N+k] = 1


        current = index+1
        #add constraint linking exit to taxes node! = 3N
        for country in range(0,N):
            for dst in range(0,N):
                A[current][N*c_len + 3*N +country*3] = (1-self.whtI[country][dst])
                A[current][country*c_len+9+dst] = -self.whtI[country][dst]

                A[current+1][N*c_len + 3*N +country*3+1]= (1-self.whtD[country][dst])
                A[current+1][country*c_len+9+N+dst] = -self.whtD[country][dst]

                A[current+2][N*c_len + 3*N +country*3+2]= (1-self.whtR[country][dst])
                A[current+2][country*c_len+9+2*N+dst] = -self.whtR[country][dst]
                current += 3

        self.InequalityMatrix = A
    def GlobalEqualityMatrix(self,src):
        n_nodes = len(list(self.node2Way.keys()))
        N = int(n_nodes/6)
        # demand vector
        # allocate memory
        c_len = 9+3*N
        A = np.full((6*N+2*(N-1)+ 3*N +1 ,N*c_len+3*N + 3*N+N +1),0.0)
        for k in range(0,N):
            i = k
            j = i*6
            # flow conservation for ENTRY NODES
            # interest
            A[j][range(i*c_len,i*c_len+3)] = [-1,-1,-1]
            A[j][range(9+i,N*c_len+9+i,c_len)] = np.full((1,N),1,dtype = int)
            A[j][N*c_len+3*k] = -1
            A[j][i*c_len+9+i] = 0
            # dividend
            A[j+1][range(i*c_len+3,i*c_len+6)] = [-1,-1,-1]
            A[j+1][range(9+N+i,N*c_len+9+N+i,c_len)] = np.full((1,N),1,dtype = int)
            A[j+1][N*c_len+3*k+1] = -1
            A[j+1][i*c_len+N+9+i] = 0
            # royalties
            A[j+2][range(i*c_len+6,i*c_len+9)] = [-1,-1,-1]
            A[j+2][range(9+2*N+i,N*c_len+9+2*N+i,c_len)] = np.full((1,N),1,dtype = int)
            A[j+2][N*c_len+3*k+2] = -1
            A[j+2][i*c_len+2*N+9+i] = 0
            # flow conservation for EXIT NODES
            #interest
            A[j+3][range(i*c_len,i*c_len+7,3)] = [1,1,1]
            A[j+3][range(9+i*c_len,i*c_len+9+N)] = np.full((1,N),-1,dtype = int)
            A[j+3][i*c_len+9+i] = 0
            # dividend
            A[j+4][range(i*c_len+1,i*c_len+8,3)] = [1,1,1]
            A[j+4][range(9+i*c_len+N,i*c_len+9+2*N)] = [-1]*N#np.full((1,N),-1,dtype = int)
            A[j+4][i*c_len+9+i+N] = 0
            A[j+4][N*c_len+3*N+3*N+i] = -1
            # royalties
            A[j+5][range(i*c_len+2,i*c_len+9,3)] = [1,1,1]
            A[j+5][range(9+i*c_len+2*N,i*c_len+9+3*N)] = np.full((1,N),-1,dtype = int)
            A[j+5][i*c_len+9+i+2*N] = 0

        current = 6*N
        for i in range(0,N):# no tranfsormation into royalties except at source
            if i+1 !=src:
                A[current][i*c_len + 2] = 1
                A[current+1][i*c_len + 5] = 1
                current += 2
        #add constraint linking entry to taxes node! = 3N
        for country in range(0,N):
            #interests
            A[current][country*c_len + 1] = -self.cit[country+1]
            A[current][N*c_len + country*3] = (1-self.cit[country+1])
            #dividends
            A[current+1][country*c_len + 3+ 1] = -self.cit[country+1]*self.xmp[country+1]
            A[current+1][N*c_len + country*3 + 1]= 1-self.cit[country+1]*self.xmp[country+1]
            #royalties
            A[current+2][country*c_len + 6+ 1] = -self.cit[country+1]*self.pb[country+1]
            A[current+2][N*c_len + country*3 +2] = 1-self.cit[country+1]*self.pb[country+1]
            current += 3

        A[current][range(N*c_len,N*c_len+3*N)] = [1]*3*N
        A[current][range(N*c_len+3*N,N*c_len+3*N+3*N)] = [1]*3*N
        A[current][N*c_len+3*N+3*N+N] = -1

        self.EqualityMatrix = A
